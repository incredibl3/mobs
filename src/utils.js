// Built-in function
var floor = Math.floor;
var random = Math.random;

exports.rollFloat = function (mn, mx) { return mn + random() * (mx - mn); };
exports.rollInt = function (mn, mx) { return floor(mn + random() * (1 + mx - mn)); };

exports.interceptOnCircle = function(p1, p2, c, r) {
    //p1 is the first line point
    //p2 is the second line point
    //c is the circle's center
    //r is the circle's radius

    var p3 = {x:p1.x - c.x, y:p1.y - c.y}; //shifted line points
    var p4 = {x:p2.x - c.x, y:p2.y - c.y};

    var m = (p4.y - p3.y) / (p4.x - p3.x); //slope of the line
    var b = p3.y - m * p3.x; //y-intercept of line

    var underRadical = Math.pow(r,2)*Math.pow(m,2) + Math.pow(r,2) - Math.pow(b,2); //the value under the square root sign 

    if (underRadical < 0) {
        //line completely missed
        return false;
    } else {
        var t1 = (-m*b + Math.sqrt(underRadical))/(Math.pow(m,2) + 1); //one of the intercept x's
        var t2 = (-m*b - Math.sqrt(underRadical))/(Math.pow(m,2) + 1); //other intercept's x
        var i1 = {x:t1+c.x, y:m*t1+b+c.y}; //intercept point 1
        var i2 = {x:t2+c.x, y:m*t2+b+c.y}; //intercept point 2

        // for this special case where the center of the circle is the starting point of the line
        // we return the intercept point which is in the line
        var distance1 = Math.sqrt((i1.x - p2.x) * (i1.x - p2.x) + (i1.y - p2.y) * (i1.y - p2.y));
        var distance2 = Math.sqrt((i2.x - p2.x) * (i2.x - p2.x) + (i2.y - p2.y) * (i2.y - p2.y));

        if (distance2 > distance1)
        	return i1;
        else 
        	return i2;
    }
};

exports.distance = function(x1, x2, y1, y2) {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
};

exports.validatePoint = function(p, max_width, max_height) {
    if (p.x < 0) p.x = 0;
    if (p.x > max_width) p.x = max_width;
    if (p.y < 0) p.y = 0;
    if (p.y > max_height) p.y = max_height;

    return p;
};