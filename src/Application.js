import device;
import animate;
import ui.View as View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import entities.Entity as Entity;
import entities.EntityPool as EntityPool;

import src.config as config;
import src.utils as utils;
import src.lib.GameOverView as GameOverView;
import src.Levels as Levels;

var app;
var BG_WIDTH = 1024;
var BG_HEIGHT = 576;
var SHOW_HIT_BOUNDS = config.debug;
var MAX_TICK = 100;
var GAME_OVER_DELAY = 4000;

exports = Class(GC.Application, function () {

  this.initUI = function () {
    app = this;
    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    // blocks player input to avoid traversing game elements' view hierarchy
    this.bgLayer = new View({
      parent: this.view,
      // x: this.view.style.width - BG_WIDTH,
      x: 0,
      backgroundColor: '#f9f8ee',
      width: this.view.style.width,
      height: BG_HEIGHT
    });

    this.player = new Player({parent: this.bgLayer});

    this.bgLayer.on('InputSelect', bind(this, function(event, point) {
      if (!this.model.gameOver)
        this.player.onAttack(point);
    }));

    this.levels = new Levels(this);
    this.enemies = new Enemies({ parent: this.bgLayer });
    this.arrows = new Arrows({ parent: this.bgLayer });

    // a 0.5 opacity dark overley at game over
    this.gameOverView = new GameOverView({
      parent: this.view,
      app: this,
      width: this.view.style.width,
      height: BG_HEIGHT,
      // x: this.view.style.width - BG_WIDTH,
      x: 0,
      y: 0,
      anchorX: this.view.style.width / 2,
      anchorY: BG_HEIGHT / 2,
      zIndex: 9999
    });
    this.gameOverView.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
    this.gameOverView.style.visible = false;

    // Level UI
    this.LevelUI = new UI({
      superview: this.bgLayer,
      x: 0,
      y: 0,
      width: 150,
      height: 50
    });
  };

  this.launchUI = function () {
    this.reset();
  };

  this.onGameOverReset = function() {
    if (this.model.isWaitingReset) {
      this.reset();
    }
  };

  this.reset = function() {
    this.model = {
      timeMult: 1,
      gameOver: false,
      isWaitingReset: false
    };
    animate.setTimeMult(this.model.timeMult);

    this.bgLayer.style.opacity = 1;
    this.gameOverView.style.visible = false;

    this.levels.reset();

    this.player.reset(config.player);
    this.enemies.reset();
    this.arrows.reset();
  };

  this.onGameOver = function() {
    if (!this.model.gameOver) {
      this.model.isWaitingReset = false;
      this.model.timeMult = 0.02;
      this.model.gameOver = true;
    }
  };

  this.clearAllAnimations = function() {
    this.player.animator.clear();
    this.enemies.clearAllAnimations()
  };

  /**
   * tick
   * ~ called automatically by devkit for each frame
   * ~ updates all game elements by delta time, dt
   */
  this.tick = function(dt) {
    // speed up or slow down the passage of time
    if (this.model.gameOver) {
      if (this.model.timeMult < 0.3)
        this.model.timeMult += 0.002;

      if (this.model.timeMult >= 0.3 && this.model.isWaitingReset == false) {
        if (this.gameOverView.style.visible ==  false)
          this.gameOverView.style.visible = true;
        this.clearAllAnimations();
        this.model.isWaitingReset = true;
      }
    }

    if (this.model.isWaitingReset)
      return;

    dt = Math.min(this.model.timeMult * dt, MAX_TICK);
    animate.setTimeMult(this.model.timeMult);

    this.enemies.update(dt);
    this.arrows.update(dt);
    this.player.update(dt);

    // Check Collistion here
    this.player.checkCollision();

    // Finalize the levels
    this.levels.update(dt);
  };

  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});

var Player = Class(Entity, function(supr) {
  var sup = Entity.prototype;

  this.init = function(opts) {
    sup.init.call(this, opts);
    this.animator = animate(this);
  };

  this.reset = function(opts) {
    opts = merge({x: app.bgLayer.style.width / 2, y: BG_HEIGHT / 2}, opts);
    sup.reset.call(this, opts);
    this.name = opts.id;
    this.level = 1;
    this.currentXP = 0;
    this.attack_power = 0;
    SHOW_HIT_BOUNDS && this.view.showHitBounds();
    this.move();
    this.lastEnemyHits = [];
  };

  this.move = function() {
    var _x = utils.rollFloat(0, app.bgLayer.style.width - this.width);
    var _y = utils.rollFloat(0, BG_HEIGHT - this.height);

    var distance = utils.distance(_x, this.view.x, _y, this.view.y)
    var time = distance / config.player.speed;
    this.animator.now({x: _x, y: _y}, time, animate.linear).then(bind(this, function() {
      this.move();
    }));
  };

  this.onAttack = function(point) {
    this.animator.clear();

    if (point.x + this.width >= app.bgLayer.style.width) {
      point.x = app.bgLayer.style.width - this.width;
    } else if (point.x <= 0) {
      point.x = 0;
    }

    if (point.y + this.height >= BG_HEIGHT) {
      point.y = BG_HEIGHT - this.height;
    } else if (point.y <= 0) {
      point.y = 0;
    }

    var distance = utils.distance(this.x, point.x, this.y, point.y);
    var interception;
    var duration;
    if (distance <= config.player.attack_radius) {
      interception = point;
      duration = config.player.attack_duration * (distance / config.player.attack_radius);
    }
    else {
      interception = utils.interceptOnCircle(this, point, this, config.player.attack_radius);
      interception = utils.validatePoint(interception, app.bgLayer.style.width, BG_HEIGHT);
      duration = config.player.attack_duration;
    }

    if (distance == 0) {
      return;
    } else {
      this.vx = this.vy = 0;
    }

    this.attack_power = config.player.attack_power;
    var start_point =  {x: this.x, y: this.y};
    this.animator.now({x: interception.x, y: interception.y}, duration, animate.easeOut).wait(200).then(bind(this, function() {
      this.attack_power = 0;
      var deltaX = this.x - start_point.x;
      var deltaY = this.y - start_point.y;
      if (deltaX == 0) {
        this.vy = config.player.base_speed;
        this.vx = 0;
      }

      if (deltaY == 0) {
        this.vx = config.player.base_speed;
        this.vy = 0;
      }

      if (Math.abs(deltaX) > Math.abs(deltaY)) {
        this.vx = (deltaX > 0) ? config.player.base_speed : -config.player.base_speed;
        this.vy = this.vx * (deltaY / deltaX);
      } else {
        this.vy = (deltaY > 0) ? config.player.base_speed : -config.player.base_speed;
        this.vx = this.vy * (deltaX / deltaY);        
      }
    }));
  };

  this.update = function(dt) {
    sup.update.call(this, dt);

    if ((this.x + this.width >= app.bgLayer.style.width && this.vx > 0)|| (this.x <= 0 && this.vx < 0)) {
      this.vx = -this.vx;
    }
    if ((this.y + this.height >= BG_HEIGHT && this.vy > 0) || (this.y <=0 && this.vy < 0)) {
      this.vy = -this.vy;
    }
  };

  this.checkCollision = function() {
    var hits = app.enemies.getAllCollidingEntities(this);

    if (hits.length > 0) {
      // Clear hit enemy that already hit in the last update call
      for (var i = 0; i < this.lastEnemyHits.length; i++) {
        var hit = this.lastEnemyHits[i];
        for (var j = 0; j < hits.length; j++) {
          var hit2 = hits[j];
          if (hit.uid == hit2.uid) {
            hits.splice(j, 1);
            break;
          }
        }
      }

      while (hits.length > 0) {
        var hit = hits[0];

        if (hit.power <= this.attack_power || hit.AI.attack.attack_type != 'collision') {
          hits.splice(0, 1);
          if (this.attack_power > 0) {
            this.addXP(hit.typeOpts.killedXP);
            hit.destroy();
          }
        } else {
          this.onDamageReceived();
          hits.splice(0, 1);
          this.lastEnemyHits.push[hit];
        }
      }
    }

    var arrow_hit = app.arrows.getAllCollidingEntities(this);
    while (arrow_hit.length > 0) {
      var arrow = arrow_hit[0];
      arrow_hit.splice(0, 1);
      arrow.animator.clear();
      arrow.destroy();
      this.onDamageReceived();
    }
  };

  this.onDamageReceived = function() {
    app.onGameOver();
  };

  this.addXP = function(xp) {
    this.currentXP += xp;

    while (this.currentXP >= Math.pow(2, this.level)) {
      this.level += 1;
      app.LevelUI.increaseLevel(this.level);
    }
  };
});

var Enemy = Class(Entity, function() {
  var sup = Entity.prototype;

  this.init = function(opts) {
    sup.init.call(this, opts);
    this.animator = animate(this);

    this.talkText = new TextView({
      superview: this.view,
      x: -75,
      y: -30,
      size: 15,
      width: 200,
      height: 30,
      text: "",
      horizontalAlign: 'center',
      verticalAlign: 'middle'
    });
    this.talkText.style.visible = false;
  }

  this.reset = function(opts) {
    sup.reset.call(this, opts);

    this.typeOpts = opts;
    this.name = opts.id;
    this.AI = opts.AI;
    this.view.style.scale = 1;
    this.view.style.width = 50;
    this.view.style.height = 50;

    this.isAttacking = false;
    this.isAnimating = false;
    this.power = this.AI.attack.attack_startPower;

    this.willTalk = (Math.random() <= this.AI.talk.talk_rate) ? Date.now() : -1;
    if (this.willTalk != -1) {
      this.talkText.setText(this.AI.talk.talk_words[Math.floor(Math.random() * this.AI.talk.talk_words.length)]);
      this.talkText.style.visible = true;
    } else {
      this.talkText.style.visible = false;
    }
  }

  this.update = function(dt) {
    sup.update.call(this, dt);

    if (!this.isAttacking) {
      this.move(app.player, app.model.timeMult);
      this.PerformAttackCheck();
    } else {
      if (this.isAnimating == false) {
        this.AI.attack.action[this.current_step](this, app.player);
      }
      if (this.power > 0) 
        this.power -= dt;
    }
    if (this.willTalk != -1) {
      if (this.talkText.style.visible == false ) {
        var between = utils.rollFloat(this.AI.talk.talk_wait[0], this.AI.talk.talk_wait[1]);
        if (Date.now() - this.willTalk > between) {
          this.willTalk = Date.now();
          this.talkText.style.visible = true;
          this.talkText.setText(this.AI.talk.talk_words[Math.floor(Math.random() * this.AI.talk.talk_words.length)]);
        }
      } else {
        if (Date.now() - this.willTalk > this.AI.talk.talk_duration) {
          this.talkText.style.visible = false;
        } else {
        }
      }
    }
  }

  this.PerformAttackCheck = function() {
    if (Date.now() - this.lastAttackAt < 1500 || app.model.gameOver)
      return;

    var distance = utils.distance(this.view.x, app.player.view.x, this.view.y, app.player.view.y);
    if (distance < this.AI.attack.attack_range) {
      this.current_step = 0;
      this.targetAttackPoint = {x: app.player.x, y: app.player.y};
      this.animator.clear(); // Clear moving stage
      this.typeOpts && this.AI.attack.action[this.current_step](this, app.player);
    };
  }

  this.move = function(player, timeMult) {
    this.current_step = 0;
    this.typeOpts && this.AI.move.action[this.current_step](this, player, timeMult);    
  }

  this.releaseArrow = function(player) {
    var opts = this.typeOpts.arrow[Math.floor(Math.random() * this.typeOpts.arrow.length)];
    var arrow = app.arrows.obtain(merge({x: this.x + this.width / 2, y: this.y + this.height / 2}, opts));
    var startpoint = {x: this.x + this.width / 2, y: this.y + this.height / 2};
    var endpoint = utils.interceptOnCircle(startpoint, player.view, startpoint, opts.range);
    arrow.animator.now({x: endpoint.x, y: endpoint.y}, opts.duration, animate.linear).then(bind(this, function() {
      arrow.destroy();
    }));
  }

  this.talk = function() {

  }  
});

var Enemies = Class(EntityPool, function() {
  var sup = EntityPool.prototype;

  this.init = function(opts) {
    opts.ctor = Enemy;
    sup.init.call(this, opts);
  };

  this.reset = function() {
    sup.reset.call(this);
    this.spawn();
  };

  this.update = function(dt) {
    sup.update.call(this, dt);

    if (Date.now() - this.lastSpawn > config.enemies.spawnTime)
      this.spawn();
  };

  this.clearAllAnimations = function() {
    this.forEachActiveEntity(function(enemy, i) {
      enemy.animator.clear();
    });
  };

  this.spawn = function() {
    if (this.getActiveCount() < config.enemies.spawnMax) {
      var done = false;
      var type = config.enemies.types[Math.floor(Math.random() * config.enemies.types.length)];

      var _x, _y;
      while (!done) {
        _x = utils.rollFloat(0, app.bgLayer.style.width - type.viewOpts.width);
        _y = utils.rollFloat(0, BG_HEIGHT - type.viewOpts.height);

        if (utils.distance(_x, app.player.x, _y, app.player.y) > 150)
          done = true;
      }
      type = merge({x: _x, y: _y}, type);

      var enemy = this.obtain(type);
      SHOW_HIT_BOUNDS && enemy.view.showHitBounds();
      this.lastSpawn = Date.now();
      enemy.lastAttackAt = this.lastSpawn;
    }
  };
});

var Arrow = Class(Entity, function() {
  var sup = Entity.prototype;

  this.init = function(opts) {
    sup.init.call(this, opts);
    this.animator = animate(this);
  }

  this.reset = function(opts) {
    sup.reset.call(this, opts);
  }
});

var Arrows = Class(EntityPool, function() {
  var sup = EntityPool.prototype;

  this.init = function(opts) {
    opts.ctor = Arrow;
    sup.init.call(this, opts);
  };

  this.reset = function() {
    sup.reset.call(this);
  };

  this.update = function(dt) {
    sup.update.call(this, dt);
  };
});

var UI = new Class(View, function(supr) {
  this.init = function(opts) {
    supr(this, 'init', [opts]);

    this.buildView(opts);
  };

  this.buildView = function(opts) {
    this.charFace = new ImageView({
      superview: this,
      x: 0,
      y: 0,
      width: 50, 
      height: 50,
      image: 'resources/images/bird.png'
    });

    this.levelText = new TextView({
      superview: this,
      x: 50,
      y: 0,
      width: 100,
      height: 50,
      text: "Level 1",
      horizontalAlign: 'center',
      verticalAlign: 'middle'
    });
  };

  this.increaseLevel = function(toLevel) {
    this.levelText.setText("Level " + toLevel);
  };
 })