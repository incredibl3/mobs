import device;
import animate;
import src.utils as utils;

var BG_WIDTH = 1024;
var BG_HEIGHT = 576;

exports = {
	debug: false,
	player: {
		id: 'player',
		speed: 0.1,	// not use anymore
		base_speed: 0.14,
		attack_radius: 300,
		attack_duration: 300,
		attack_power: 900,
		x: 0,
		y: 0,
		hitOpts: {
		  offsetX: 0,
		  offsetY: 0,
	      width: 50,
	      height: 50
	    },
	    viewOpts: {
	      width: 50,
	      height: 50,
	      url: "resources/images/bird.png"
	    }      
	},
	enemies: {
		spawnMax: 12,
		spawnTime: 2000,
		types: [
			{
				id: 'pikeman',
				x: 0,
				y: 0,
				killedXP: 3,
				hitOpts: {
				  offsetX: 0,
				  offsetY: 0,
			      width: 50,
			      height: 50
			    },
			    viewOpts: {
			      width: 50,
			      height: 50,
			      scale: 1,
			      anchorX: 25,
			      anchorY: 25,
			      url: "resources/images/pikeman.png"
			    },
			    AI: {
			    	attack: {
				    	attack_range: 100,
				    	attack_type: 'collision',
				    	attack_maxPower: 1000,
				    	attack_startPower: 0,
			    		total_step: 3,
			    		action: [
			    			// Prepare Attack
				    		function(enemy, player) {
				    			enemy.isAttacking = true;
				    			enemy.isAnimating = true;
				    			animate(enemy.view).now({scale: 1.5}, 250, animate.linear).then(function() {
				    			}).wait(100).then(function() {
				    				enemy.isAnimating = false;
				    				enemy.current_step++;
				    			});
				    		},
				    		// Max Attack Power 
				    		function(enemy, player) {
				    			enemy.isAnimating = true;
				    			enemy.power = enemy.typeOpts.AI.attack.attack_maxPower;
				    			animate(enemy.view).now({scale: 1}, 250, animate.linear).then(function() {
				    			}).wait(100).then(function() {
				    				enemy.isAnimating = false;
				    				enemy.current_step++;				    				
				    			});
				    		},
				    		// This attack type is collision, so move the enemy to the player
				    		function(enemy, player) {
				    			enemy.isAnimating = true;
				    			enemy.isAttacking = true;
				    			// var interception = utils.interceptOnCircle(enemy.view, enemy.targetAttackPoint, enemy.view, enemy.AI.attack.attack_range);
				    			var interception = utils.interceptOnCircle(enemy.view, player.view, enemy.view, enemy.AI.attack.attack_range);
							    enemy.animator.now({x: interception.x, y: interception.y}, 400, animate.easeOut)
							    .wait(400)
							    .then(function() {
							    	enemy.isAnimating = false;
							    	enemy.isAttacking = false;
							    	enemy.lastAttackAt = Date.now();
							    }).then(function() {
							    	enemy.current_step = 0;
							    	// enemy.move(enemy, player);
							    });
							}
			    		]
			    	},
			    	move: {
			    		movement_speed: 3,
			    		total_step: 1,
			    		action: [
			    			function(enemy, player, timeMult) {
			    				var distance = utils.distance(enemy.x, player.x, enemy.y, enemy.y);
			    				if (distance <= 20)
			    					return;

				    			var interception = utils.interceptOnCircle(enemy.view, player, enemy.view, enemy.AI.move.movement_speed * timeMult);
				    			enemy.x = interception.x;
				    			enemy.y = interception.y;
			    			}
			    		]
			    	},
			    	talk: {
			    		talk_rate: 0.2,
			    		talk_duration: 2500,
			    		talk_wait: [5000, 7500],
			    		talk_words: [
			    			"Really? No treasure ?",
			    			"Come at me !",
			    			"Hope I will level up .",
			    			"Is there a princess anYwhere ?",
			    			"You're supposed to be a boss ?",
			    			"This dungeon sucks ."
			    		],
			    		action: [
			    			function(enemy) {
			    				enemy.talk();
			    			}
			    		]
			    	}
				}
			},
			{
				id: 'archer',
				x: 0,
				y: 0,
				killedXP: 2,
				hitOpts: {
				  offsetX: 0,
				  offsetY: 0,
			      width: 50,
			      height: 50
			    },
			    viewOpts: {
			      width: 50,
			      height: 50,
			      scale: 1,
			      anchorX: 25,
			      anchorY: 25,
			      url: "resources/images/archer.png"
			    },
			    arrow: [
			    	{
			    		id: 'arrow1',
						x: 0,
						y: 0,
						range: 400,
						duration: 1200,
						hitOpts: {
						  offsetX: 0,
						  offsetY: 0,
					      width: 15,
					      height: 6
					    },
					    viewOpts: {
					      width: 15,
					      height: 6,
					      scale: 1,
					      url: "resources/images/arrow.png"
					    },			    		
					}
			    ],
			    AI: {
			    	attack: {
				    	attack_range: 150,
				    	attack_type: 'range',
				    	attack_maxPower: 1000,
			    		total_step: 3,
			    		action: [
			    			// Prepare Attack
				    		function(enemy, player) {
				    			enemy.isAttacking = true;
				    			enemy.isAnimating = true;
				    			animate(enemy.view).now({scale: 1.5}, 250, animate.linear).then(function() {
				    			}).wait(100).then(function() {
				    				enemy.isAnimating = false;
				    				enemy.current_step++;
				    			});
				    		},
				    		// Max Attack Power 
				    		function(enemy, player) {
				    			enemy.isAnimating = true;
				    			enemy.power = enemy.typeOpts.AI.attack.attack_maxPower;
				    			animate(enemy.view).now({scale: 1}, 250, animate.linear).then(function() {
				    			}).wait(100).then(function() {
				    				enemy.isAnimating = false;
				    				enemy.current_step++;				    				
				    			});
				    		},
				    		// This attack type is range, so release the bullet
				    		function(enemy, player) {
				    			enemy.releaseArrow(player);
				    			enemy.lastAttackAt = Date.now();
						    	enemy.isAnimating = false;
						    	enemy.isAttacking = false;
				    			// enemy.move(player);
							}
			    		]
			    	},
			    	move: {
			    		movement_speed: 3,
			    		total_step: 1,
			    		action: [
			    			function(enemy, player, timeMult) {
			    				var distance = utils.distance(enemy.x, player.x, enemy.y, enemy.y);
			    				if (distance <= 20)
			    					return;

				    			var interception = utils.interceptOnCircle(enemy.view, player, enemy.view, enemy.AI.move.movement_speed * timeMult);
				    			enemy.x = interception.x;
				    			enemy.y = interception.y;
			    			}
			    		]
			    	},
			    	talk: {
			    		talk_rate: 0.2,
			    		talk_duration: 2500,
			    		talk_wait: [5000, 7500],
			    		talk_words: [
			    			"Please stop moving .",
			    			"I saw him first .",
			    			"Do you drop any artifact ?"
			    		],
			    		action: [
			    			function(enemy) {
			    				enemy.talk();
			    			}
			    		]
			    	}			    	
			    }				
			}
	    ]    
	}	
}