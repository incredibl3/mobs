import ui.View as View;
import ui.TextView as TextView;

exports = Class(View, function(supr) {
  var app;

  this.init = function(opts) {
    supr(this, 'init', [opts]);

    app = opts.app;
    this.buildView(opts);
  };

  this.buildView = function(opts) {
    this.gameOverTxt = new TextView({
      superview: this,
      x: 0,
      y: 0,
      width: this.style.width,
      height: this.style.height / 2,
      size: 80,
      fontWeight: 'bold',
      horizontalAlign: "center",
      verticalAlign: "middle",
      text: "Game Over!",
      color: "#766e65",
      zIndex: 10000
    });

    this.tryAgain = new TextView({
      superview: this,
      x: this.style.width / 4,
      y: this.style.height / 2,
      width: this.style.width / 2,
      height: this.style.height / 8,
      size: 60,
      color: "#766e65",
      text: "Try Again"
    });

    this.on('InputSelect', function() {
      app.onGameOverReset();
    });
  };
})